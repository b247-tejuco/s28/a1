//part 1
db.rooms.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "a simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

//part 2
db.rooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "a room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},

	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "a room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
]);

//part 3
db.rooms.find({name: "double"})


//part 4
db.rooms.updateOne (
	{name: "queen"},
	{
		$set: {
			name: "queen",
			accomodates: 4,
			price: 4000,
			description: "a room with a queen sized bed perfect for a simple getaway",
			rooms_available: 0,
			isAvailable: false
		}
	}

)

//part 5
db.rooms.deleteMany(
		{rooms_available: 0}
	)
